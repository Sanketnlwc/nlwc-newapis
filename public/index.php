<?php
header('Access-Control-Allow-Origin: *');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//timezone setting
date_default_timezone_set('Asia/Kolkata');
error_log(E_ALL);
require '../vendor/autoload.php';
spl_autoload_register(function ($classname) {
    require ("../classes/" . $classname . ".php");
});
require '../src/settings.php';
$app = new \Slim\App(["settings" => $config]);
require '../src/dependencies.php';
$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});
$app->get('/login', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->login($data['uname'], $data['password']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/contactusform', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->contactEmailSend($data['name'],$data['email'], $data['subject'], $data['content']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/status', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->status($data['username']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/tales', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new tales($this->db);
    $res = $entrypoint->alldata();
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/cricstore', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $cricstore = new cricstore($this->db);
    $res = $cricstore->getAlldata();
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/submitCricstoreOrder', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $cricstore = new cricstore($this->db);
    $res = $cricstore->postOrder($data);
    $response->getBody()->write(json_encode($res));
    return $request;
});


$app->get('/walletReset', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->walletReset($data['username']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/predict', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $matches = $predict->getPredictMatches($data['username']);
    $finishedMatches = $predict->getPredictFinishedMatches();
    $getMatchPoints = $predict -> getMatchPoints();           
    $tournaments = $predict -> getTournaments();        
     $array = array("matches"=>$matches,'finishedMatches'=>$finishedMatches,'matchPoints'=>$getMatchPoints,'tournament'=>$tournaments);
     
    $response->getBody()->write(json_encode($array));
    return $response;

    // $response->getBody()->write(json_encode($matches));
    // return $response;
});


$app->get('/fetchPlayers', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->fetchPlayers($data['team1'],$data['team2']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/fetchPredictions', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->fetchPredictions($data['srno'],$data['type']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/addPrediction', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->addPrediction($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/updatePrediction', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->updatePrediction($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/fetchTeam', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->fetchTeam($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});



$app->get('/checkPredict', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->checkPredict($data['id']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/insertpredict', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->fromDiffDB($data['table'],$data['match']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/matchPoint/{type}', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $home = new home($this->db);
    if($type=="tournament")
        $res = $home->getPrediction($data['id'],'tournament');
    else $res = $home->getPrediction($data['id']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/predictinsert', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $predict = new predict($this->db);
    $res = $predict->insert($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/blogfetch', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $blogs = new blogs($this->db);
    $res = $blogs->fetchallBlogsData($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/countdown', function (Request $request, Response $response) {
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->fetchCountdown();
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->post('/blogs/{type}', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $blogs = new blogs($this->db);
    if($type=='like')
        $res = $blogs->likeUpdate($data);
    else $res = $blogs->commentUpdate($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});


$app->get('/register', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->register($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/fblogin', function (Request $request, Response $response) {
    $type = $request->getAttribute('type');
    $data = $request->getQueryParams();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->fblogin($data);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->post('/picSubmit', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->updateImage($_FILES['image'],$data['username']);
    return $res;
});

$app->post('/filesUpload', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $blog = new blog($this->db);
    $res = $blog->uploadImages($_FILES['image'],$data['username']);
    return $res;
});
$app->get('/home', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $home = new home($this->db);
    $res = $home->getAlldata();
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/bannerinfo', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $bannerinfo = new bannerinfo($this->db);
    $res = $bannerinfo->getAlldata();
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/rssfeeds', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $rssfeed = new rssfeed($this->db);
    $res = $rssfeed->getAlldata($data['countryId']);
    $response->getBody()->write(json_encode($res));
    return $response;
});


$app->get('/blogs', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $blogs = new blogs($this->db);
    $searchQuery=isset($data['searchQuery'])?$data['searchQuery']:null;
    $res = $blogs->getAlldata($data['username'],$data['search'],$searchQuery,$data['isStory']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/header', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $headerData = new headersData($this->db);
    $res = $headerData->getAlldata($data['username']);
    $response->getBody()->write(json_encode($res));
    return $response;
});

$app->get('/blogsWrite', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $postData = $request->getParsedBody();
    $blogs = new blogs($this->db);
    $res = $blogs->postBlogData($data['username'],$data['title'],$data['blogContent']);
    $response->getBody()->write(json_encode($res));
    return $response;
});





$app->get('/getstudent', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $entrypoint = new student($this->db);
    $res = $entrypoint->retrieve($data['name']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/getBatch', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $entrypoint = new student($this->db);
    $res = $entrypoint->getBatch($data['batch'],$data['standard']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/getpaymentlist', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $entrypoint = new student($this->db);
    $res = $entrypoint->payment();
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/forget/{email}', function (Request $request, Response $response) {
    $email = $request->getAttribute('email');
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->forget($email);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/changepassword/{password}/{username}', function (Request $request, Response $response) {
    $username = $request->getAttribute('username');
    $password = $request->getAttribute('password');
    $entrypoint = new entrypoint($this->db);
    $res = $entrypoint->changePassword($username,$password);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/addcourse', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $entrypoint = new course($this->db);
    $res = $entrypoint->add($data['baseStandard'],$data['courseName'],$data['batch'],$data['fees']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/course', function (Request $request, Response $response) {
    $entrypoint = new course($this->db);
    $res = $entrypoint->retrieve();
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/enroll', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $enroll = new enroll($this->db);
    $res = $enroll->subscribe($data['userId'], $data['courseId'], $data['paymentType']);
    $res = $enroll->add($data['userId'], $res, $data['payment']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/makePayment', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $enroll = new enroll($this->db);
    $res = $enroll->add($data['userId'], $data['enrollid'], $data['payment']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->get('/addLecture', function (Request $request, Response $response) {
    $data = $request->getQueryParams();
    $lecture = new lecture($this->db);
    $res = $lecture->addlecture($data['courseId'],$data['professorId'],$data['duration']);
    $response->getBody()->write(json_encode($res));
    return $response;
});
$app->run();
