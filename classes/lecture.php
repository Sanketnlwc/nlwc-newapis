<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class lecture {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function addlecture($course, $professor, $duration) {
        $sql = "insert into `lecture` (`professorId`,`duration`,`courseId`,`dateTime`) values (:professorId,:duration,:courseId,:dateTime);";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':professorId', $professor);
        $stmt->bindParam(':duration', $duration);
        $stmt->bindParam(':courseId', $course);
        $tempDate = new DateTime();
        date_default_timezone_set('	Asia/Kolkata');
        $tempDate = $tempDate->format('y-m-d H:i:s');
        $stmt->bindParam(':dateTime', $tempDate);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $stmt->closeCursor();
        return 'success';
    }

}
