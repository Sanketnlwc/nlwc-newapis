<?php

/*
 * To change this license , choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class headersData {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getAlldata($username) {
        $headerData = $this->getHeaderData($username);
        $array = array("HeaderData"=>$headerData);
       return $array;
    }

   

    public function getHeaderData($username){
            $sql = "select fname,lname,username,profile_pic,email_id  from nlwc where username=:username";
        $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':username', $username);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();

         $sql1 = "select count(*) as NumberBlogs from blogs where username=:username";
        $stmt1 = $this->db->prepare($sql1);
      $stmt1->bindParam(':username', $username);
        $result1 = $stmt1->execute();
        $er1 = $stmt1->errorInfo();
        $result1 = $stmt1->fetchAll();

         $sql2 = "select count(*) as TotalBlogs from blogs";
        $stmt2 = $this->db->prepare($sql2);
        $result2 = $stmt2->execute();
        $er2 = $stmt2->errorInfo();
        $result2 = $stmt2->fetchAll();
        
        array_push($result,$result1);
        array_push($result,$result2);
        // $helper = new helper();
        // $result = $helper->profilePic($result);
        
        return $result;
}
}
