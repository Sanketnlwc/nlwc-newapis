<?php class bannerinfo {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getAlldata() {
         $sql = "( Select * From bannerinfo)";
        $stmt = $this->db->prepare($sql);
        $result= $stmt->execute();
        $er = $stmt -> errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }
}
?>