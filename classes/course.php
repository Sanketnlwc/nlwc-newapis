<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class course {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function add($baseStandard,$courseName,$batch,$fees) {
            $sql = "insert into `courses` (`baseStandard`,`courseName`,`batch`,`fees`) values (:baseStandard,:courseName,:batch,:fees)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':baseStandard', $baseStandard);
            $stmt->bindParam(':courseName', $courseName);
            $stmt->bindParam(':batch', $batch);
            $stmt->bindParam(':fees', $fees);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            return 'Success';
    }

    public function retrieve() {
            $sql = "select * from courses";
            $stmt = $this->db->prepare($sql);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $result = $stmt->fetchAll();
            return $result;
    }
}
