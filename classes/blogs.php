<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class blogs {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getAlldata($username,$search,$title,$isStory) {
        // if($article === "true")
        //     $sql = "select fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views` from blogs b,nlwc n where n.username=b.username and b.News=1 order by b.Sr_No desc";
        // else
        //     $sql = "select fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views` from blogs b,nlwc n where n.username=b.username and b.News=0 order by b.Sr_No desc";
        
        if($search === "true")
            $sql = "select fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views`,`story` from blogs b,nlwc n where n.username=b.username and (b.title LIKE '%$title%' or b.username LIKE '%$title%')	and b.story='$isStory' and b.posted=1 order by b.Date desc";
        else
            $sql = "select fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views`,`story` from blogs b,nlwc n where n.username=b.username and  b.story='$isStory' and b.posted=1 order by b.Date desc";
           
        $stmt = $this->db->prepare($sql);
        //if($search==="true")
        //    $stmt->bindParam(':title', $title);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        // $result = $helper->profilePic($result);
        $mainUrl = "http://www.nolifewithoutcricket.com/";
        foreach($result as $index=>$innerData){
            $url = "../blogs/".$innerData['title'].".txt";
            $fh = fopen($url,'r');
            $blog = fread($fh,filesize($url));
            fclose($fh);
            $blog = strip_tags($blog);
            $str = $this->myTruncate($blog);
            $result[$index]['blogData']=$str;
            $tempData = array('username'=>$username,'srno'=>$innerData['blogSRNO']);
            $result[$index]['status']=$this->getStatus($tempData);
        }
        return $result;
  }

  public function myTruncate($input, $numwords="30", $padding="...<a class='link'>Readmore</a>"){
    $output = strtok($input, " \n");
    while(--$numwords > 0) $output .= " " . strtok(" \n");
   // if($output != $input) $output .= $padding;
    return $output;
  }

   public function fetchallBlogsData($data) {
       $this->updateView($data);
       $blogs = $this->getBlog($data);
       $likescount=$this->getLikes($data);
       $blogs[0]['status']=$this->getStatus($data);
       $blogcontent = $this->fetchBlogs($blogs[0]['title']);
       $comments=$this->getComments($blogs[0]['title']);
       $array = array('Blogs'=>$blogs,'likescount'=>$likescount,'blogcontent'=>$blogcontent,'comments'=>$comments);
       return $array;
  }

  public function getStatus($data){
      $sql = "select (case when username=:username then 1 else 0 end) status from `likes-count` where blogid=:srno";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':srno', $data['srno']);
      $stmt->bindParam(':username', $data['username']);
      $result = $stmt->execute();
      $er = $stmt->errorInfo();
      $result = $stmt->fetchAll();
      if($result)
        return $result[0]['status'];
    else return 0;
  }

  public function updateView($data){
      $sql = "UPDATE `blogs` SET `views`=`views`+1,`points`=`points`+0.1 where `Sr_No`=:srno";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':srno', $data['srno']);
      $result = $stmt->execute();
      $er = $stmt->errorInfo();
      return true;
  }

   public function likeCheck($data){
      $sql = "select count(*) from `likes-count` where username=:username and blogid=:srno";
        $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':srno', $data['srno']);
      $stmt->bindParam(':username', $data['username']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        if($result > 1)
          return 0;
          else{
           return likeUpdate($data);
          }
  }

  public function likeUpdate($data){
      $sql = "UPDATE `blogs` b SET b.`like`=`like`+1,b.`points`=b.`points`+2 where `Sr_No`=:srno;insert into `likes-count` (`username`,`blogid`) values (:username,:srno)";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':srno', $data['srno']);
      $stmt->bindParam(':username', $data['username']);
      $result = $stmt->execute();
      $er = $stmt->errorInfo();
      return true;
  }

  public function commentUpdate($data){
      $datetime = new DateTime();
      $date = $datetime->format('Y-m-d');
      $time = $datetime->format('H:i:s');
      $sql = "insert into `commenttable` (`name`,`comment`,`date`,`time`,`blogname`) values (:name,:comment,:date,:time,:title)";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':name', $data['username']);
      $stmt->bindParam(':comment', $data['comment']);
      $stmt->bindParam(':date', $date);
      $stmt->bindParam(':time', $time);
      $stmt->bindParam(':title', $data['title']);
      $result = $stmt->execute();
      $er = $stmt->errorInfo();
      return true;
  }

  public function getBlog($data){
        $sql = "select n.username Username,fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views` from blogs b,nlwc n where n.username=b.username and b.Sr_No=:Sr_No";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':Sr_No', $data['srno']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        return $result;
  }

   public function getLikes($data){
        $sql = "select count(*) from `likes-count` l,nlwc n where n.username=l.username and l.Sr_No=:Sr_No";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':Sr_No',$data['srno']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
  }

  public function fetchBlogs($title){
        $url = "../blogs/".$title.".txt";
        $fh = fopen($url,'r');
        $data = fread($fh,filesize($url));
        fclose($fh);
        return $data;
    }

  public function getComments($title)
  {
       $sql = "select `fname`,`lname`,`profile_pic`,`name`,`date`,`comment`,`comment_id` from commenttable c,nlwc n where n.username=c.name and c.blogname=:blogName";
       $stmt = $this->db->prepare($sql);
       $stmt->bindParam(':blogName',$title);
       $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        return $result;

  }

  public function uploadImages($image,$userId,$filename){
    try{
        if(move_uploaded_file($image['tmp_name'],"../../images/cricstories/$userId/$filename")){
            $file=getenv('DASHBOARD')."/images/cricstories/$userId/$filename";
            return $file;
        }
        else return false;
    } catch (Exception $e) {
        echo "Failed to get DB handle: " . $e->getMessage() . "\n";
    }
}

public function postBlogData($username,$blogTitle,$blogContent,$story)
{
    $datetime = new DateTime();
    $date = $datetime->format('Y-m-d');
    $blogurl = 'blogs/.'.$blogTitle.'.txt';
    $blogFilePath = "../blogs/".$blogTitle.".txt";
$fileName = fopen($blogFilePath, "w") or die("Unable to open file!");
fwrite($fileName, $blogContent);
  fclose($fileName);
       $sql = "insert into `blogs` (`username`,`title`,`blog_url`,`Date`,`story`) values (:username,:title,:blogurl,:date,:story)";
          $stmt = $this->db->prepare($sql);
          $stmt->bindParam(':username', $username);
          $stmt->bindParam(':title', $blogTitle);
          $stmt->bindParam(':blogurl', $blogurl);
          $stmt->bindParam(':date', $date);
          $stmt->bindParam(':story', $story);
          $result = $stmt->execute();
          $er = $stmt->errorInfo();
          $stmt->closeCursor();
          if($result) 
          return 1;
          else
          return 0;
}

}
