<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class entrypoint {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function status($username){
        $sql = "select fname, lname, n.username, email_id, mobile_no, profile_pic, wallet, Reset, id from `nlwc` n,`session` s where n.username=s.username and n.username=:username";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        // $stmt->bindParam(':ipAddress', $_SERVER["REMOTE_ADDR"]);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        $stmt->closeCursor();
        if(count($result)>0){
            return $result[0];
        }
        else return "failed";
    }

     public function fetchCountdown(){
        $sql = "select title, team1, team2, date, time from `infomatch`  where `update` = '0' order by Sr_No";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
         $stmt->closeCursor();
        return $result;
    }



    public function login($email, $pass) {
        $sql = "SELECT * FROM `nlwc` WHERE `username`=:username and password=:password";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $email);
        $stmt->bindParam(':password', $pass);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        $stmt->closeCursor();
        $this->handleSession($email,$_SERVER["REMOTE_ADDR"]);
        if(count($result)>0){
            return $result[0];
        }
        else return "0";
    }

    public function changePassword($username,$password){
         $sql = "update `nlwc` set `password`=:password where `username`=:username";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':password', $password);
            $stmt->bindParam(':username', $username);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            return $result;
    }

    public function handleSession($username,$ipaddress){
        $sql = "INSERT INTO `session` (username,ipAddress) VALUES (:username,:ipAddress) ON DUPLICATE KEY UPDATE ipAddress=:ipAddress";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':ipAddress', $ipaddress);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $stmt->closeCursor();
    }



    public function register($data) {
        $sql = "select count(*) count from nlwc where username=:username";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $data['username']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $usernameCount = $stmt->fetchAll();
        $stmt->closeCursor();
        if($usernameCount[0]['count'] != 0)
            return "0";


        $sql1 = "select count(*) count from nlwc where email_id=:email_id";
        $stmt1 = $this->db->prepare($sql1);
        $stmt1->bindParam(':email_id', $data['email']);
        $result1 = $stmt1->execute();
        $er = $stmt1->errorInfo();
        $emailCount = $stmt1->fetchAll();
        $stmt1->closeCursor();
        if($emailCount[0]['count'] != 0)
            return "1";

        if(!$studentCount[0]['count']){
            $sql = "insert into `nlwc` (`fname`,`lname`,`username`,`password`,`email_id`,`mobile_no`,`profile_pic`,`wallet`) values (:fname,:lname,:username,:password,:email_id,:mobile_no,:profile_pic,:wallet);";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':fname', $data['fname']);
            $stmt->bindParam(':lname', $data['lname']);
            $stmt->bindParam(':username', $data['username']);
            $stmt->bindParam(':password', $data['password']);
            $stmt->bindParam(':email_id', $data['email']);
            $stmt->bindParam(':mobile_no', $data['phone']);
            $profile_pic=null;
            $stmt->bindParam(':profile_pic', $profile_pic);
            $wallet=100;
            $stmt->bindParam(':wallet', $wallet);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
             $subject = "Welcome to NLWC";
             $main = "You Have Successfully Registered On NLWC<br>
             Welcome To The NLWC Family!!!!!!!
             <br> You Can Now Predict For Matches, Write Blogs etc.
			 <br>
             <h3 style='color: cornflowerblue;'> Stay Connected,Stay Updated</h3> ";
             $emailsend = new sendemail();
             $emailsend->emailSend($data['fname'], $data['email'], $subject, $main);
            $id = $this->login($data['username'], $data['password']);
            return $id;
        }
    }

    public function fblogin($data) {
        $fname = $data['fname'];
        $lname = $data['lname'];
        $email = $data['email'];
        $id = $data['id'];
        $profile_pic = 'https://graph.facebook.com/' . $id . '/picture?width=300';
        $sql = "select * from nlwc where email_id=:email_id";
        $stmt = $this->db->prepare($sql);
        if($email=="undefined"){
            $email = $fname."".$lname;
        }
        $stmt->bindParam(':email_id', $email);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $studentCount = $stmt->fetchAll();
        $helper = new helper();
        $studentCount = $helper->profilePic($studentCount);
        $stmt->closeCursor();
        if(!$studentCount){
            $username = $fname.".".$lname;
            $sql = "insert into `nlwc` (`fname`,`lname`,`username`,`email_id`,`profile_pic`) values (:fname,:lname,:username,:email_id,:profile_pic);";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':fname', $fname);
            $stmt->bindParam(':lname', $lname);
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':email_id', $email);
            $stmt->bindParam(':profile_pic', $profile_pic);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            $subject = "Welcome to NLWC";
            $main = "You Have Successfully Registered On NLWC<br>
            Welcome To The NLWC Family!!!!!!!
            <br> You Can Now Predict For Matches, Write Blogs etc.
			<br>
            <h3 style='color: cornflowerblue;'> Stay Connected,Stay Updated</h3> ";
            $emailsend = new sendemail();
            $emailsend->emailSend($fname, $email, $subject, $main);
            $this->handleSession($email,$_SERVER["REMOTE_ADDR"]);
            return $username;
        }else {
            $this->handleSession($studentCount[0]['username'],$_SERVER["REMOTE_ADDR"]);
            $sql = "update `nlwc` set `profile_pic`=:profile_pic where `email_id`=:email";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':profile_pic', $profile_pic);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            return $studentCount[0];
        }
        return "failed";
    }

    public function forget($email) {
        $sql = "SELECT * FROM `nlwc` WHERE `email_id`=:email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        if (count($result) > 0) {
            $username = $result[0]['username'];
            $temppassword = mt_rand(1,1000);
            $temppassword="temp".$temppassword;
            $sql = "update `nlwc` set `password`=:password where `email_id`=:email";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':password', $temppassword);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            $subject = "Your new temporary password";
            $main = "Your userid and temporary password is UserId : ".$username." Password : ".$temppassword.". You can login with this credentials and change the password";
            $emailsend = new sendemail();
            $emailsend->emailSend($username, $email, $subject, $main);
            return "done";
        } else {
            return "0";
        }
    }

    public function updateImage($image,$data){
        $image = $this->saveImage($image,$data);
        if($image){
            $sql = "update nlwc set profile_Pic=:profilePic where username=:userId";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':profilePic', $image);
            $stmt->bindParam(':userId', $data);
            $result = $stmt->execute();
            $id = $this->db->lastInsertId();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            return $image;
        }
        else {
            return 'failed';
        }
    }

    public function saveImage($image,$userId){
        try{
            if(move_uploaded_file($image['tmp_name'],"../../nlwc-redev/images/user/profile/$userId.jpg")){
                $file=getenv('DASHBOARD')."/images/user/profile/$userId.jpg";
                return $file;
            }
            else return false;
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }



      function contactEmailSend($name,$email,$subject,$content) {
        $from = new SendGrid\Email($name, $email);
        $to = new SendGrid\Email('Admin', 'nolifewithoutcricket@gmail.com');
        $content = new SendGrid\Content("text/html", $content);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        return 1;
    }

}
