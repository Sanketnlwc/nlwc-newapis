<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class predict {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function fromDiffDB($table,$matchId){
        $this->removeOld($matchId);
        $data = $this->getfromold($table);
        $sql = "insert into prediction (`tournamentId`,`idMatch`,`gameWeekPoint`,`player`,`username`) values (:tournamentId,:gameweekId,:point,:player,:username)";
        try{
            $stmt = $this->db->prepare($sql);
            $this->db->beginTransaction();
            foreach($data as $key=>$innerData){
                $tournamentId=1;$id=$matchId;$username = $innerData['username'];unset($innerData['username']);
                $player = json_encode($innerData);
                $stmt->bindParam(':tournamentId', $tournamentId);
                $stmt->bindParam(':gameweekId', $id);
                $stmt->bindParam(':player', $player);
                $a = "{}";
                $stmt->bindParam(':point', $a);
                $stmt->bindParam(':username', $username);
                $stmt->execute();
            }
            $this->db->commit();
        }catch (Exception $e){
            $this->db->rollback();
            echo "an error has occured";
        }
    }

    public function removeOld($matchId){
        $sql = "truncate nlwc;delete from prediction where idMatch=:matchId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':matchId', $matchId);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $pdo = new PDO("mysql:host=nolifewithoutcricket.com;dbname=nlwc","nlwc07","Nlwc@1Admin");
        $sql = "select * from nlwc";
        $stmt = $pdo->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $sql = "insert into nlwc (fname, lname, username, password, email_id, mobile_no, profile_pic, wallet, Reset) values (:fname, :lname, :username, :password, :email_id, :mobile_no, :profile_pic, :wallet, :Reset)";
        try{
            $stmt = $this->db->prepare($sql);
            $this->db->beginTransaction();
            foreach($result as $key=>$innerData){
                $stmt->bindParam(':fname', $innerData['fname']);
                $stmt->bindParam(':lname', $innerData['lname']);
                $stmt->bindParam(':username', $innerData['username']);
                $stmt->bindParam(':password', $innerData['password']);
                $stmt->bindParam(':email_id', $innerData['email_id']);
                $stmt->bindParam(':mobile_no', $innerData['mobile_no']);
                $stmt->bindParam(':profile_pic', $innerData['profile_pic']);
                $stmt->bindParam(':wallet', $innerData['wallet']);
                $stmt->bindParam(':Reset', $innerData['Reset']);
                $stmt->execute();
            }
            $this->db->commit();
        }catch (Exception $e){
            $this->db->rollback();
            echo "an error has occured";
        }
    }

    public function getfromold($table){
        $pdo = new PDO("mysql:host=nolifewithoutcricket.com;dbname=nlwc","nlwc07","Nlwc@1Admin");
        $resultData = array();
        for($i=1;$i<=11;$i++){
            $sql = "select username,player".$i." player,".$i." count,Role,Value,TeamName from `".$table."` t,`ipl2017_players` i where Player=player".$i;
            $stmt = $pdo->prepare($sql);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($resultData)
                $resultData = array_merge($resultData,$result);
            else $resultData=$result;
        }
        $data = $this->arraymanipulation($resultData);
        return $data;
    }

    public function arraymanipulation($prediction){
        $result = array();
        foreach($prediction as $key=>$innerData){
            $teamInitial = "";
            $team=explode(" ",$innerData['TeamName']);
            foreach($team as $key=>$teamInnerData){
                $teamInitial.=$teamInnerData[0]; 
            }
            $count = "player".$innerData['count'];
            $result[$innerData['username']][$count]=$innerData['Value']."|".$innerData['player']."|".$teamInitial."|".$innerData['Role'];
            $result[$innerData['username']]['username']=$innerData['username'];
        }
        return $result;
    }

    public function insert($data){
        $demo['id']=$data['tournamentId'];
        $fixturesDone = $this->getFixtures($demo,'done');
        $prediction = $this->getPrediction($demo,$data['username']);
        $count = count($fixturesDone)-count($prediction);
        if(count($fixturesDone)!=count($prediction)){
            if($count>0){
                $this->insertPrediction(null,$data['username'],$data['tournamentId'],$fixturesDone);
                $count=0;
            }
        }
        if($count==0){
            $this->insertPrediction($data,$data['username'],$data['tournamentId'],$data['gameweekId']);
            $this->setwallet($data['wallet'],$data['username']);
        }
        if($count<0){
            return 2;
        }
        return 1;
    }

    public function setwallet($wallet,$username){
        $sql = "update nlwc set `wallet`=:wallet where `username`=:username";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':wallet', $wallet);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        return 1;
    }

    public function checkPredict($id){
        $result = $this->getPredictionCheck($id-1);
        $sql = "INSERT IGNORE INTO prediction (tournamentId, idMatch, username, gameWeekPoint,player) VALUES (:id,:idMatch,:username,:points,:player)";
        try{
            $stmt = $this->db->prepare($sql);
            $this->db->beginTransaction();
            foreach($result as $key=>$innerData){
                $stmt->bindParam(':id', $innerData['tournamentId']);
                $stmt->bindParam(':idMatch', $id);
                $a = "{}";
                $stmt->bindParam(':points', $a);
                $stmt->bindParam(':username', $innerData['username']);
                $stmt->bindParam(':player', $innerData['player']);
                $stmt->execute();
            }
            $this->db->commit();
        }catch (Exception $e){
            $this->db->rollback();
            echo "an error has occured";
        }
    }

    public function getPredictionCheck($id){
        $sql = "select * from prediction where idMatch=:id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function insertPrediction($player,$username,$tournamentId,$gameweekid){
        if(is_array($gameweekid)){
            $sql = "insert into prediction (`tournamentId`,`idMatch`,`gameWeekPoint`,`username`) values (:tournamentId,:gameweekId,:point,:username)";
            try{
                $stmt = $this->db->prepare($sql);
                $this->db->beginTransaction();
                foreach($gameweekid as $key=>$innerData){
                    $stmt->bindParam(':tournamentId', $tournamentId);
                    $stmt->bindParam(':gameweekId', $innerData['id']);
                    $a = "{}";
                    $stmt->bindParam(':point', $a);
                    $stmt->bindParam(':username', $username);
                    $stmt->execute();
                }
                $this->db->commit();
            }catch (Exception $e){
                $this->db->rollback();
                $this->checkPredict($gameweekid[count($gameweekid)-1]['id']);
                // echo "an error has occured";
            }
        }
        else {
            $playerArray = array();
            for($i=1;$i<=11;$i++){
                // $temp = explode("|",$player['player'.$i]);
                $playerArray['player'.$i]=$player['player'.$i];
            }
            $sql = "insert into prediction (`tournamentId`,`idMatch`,`username`,`player`,`gameWeekPoint`) values (:tournamentId,:gameweekId,:username,:player,:point)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':tournamentId', $tournamentId);
            $stmt->bindParam(':gameweekId', $gameweekid);
            $stmt->bindParam(':username', $username);
            $json = json_encode($playerArray);
            $stmt->bindParam(':player', $json);
            $a = "{}";
            $stmt->bindParam(':point', $a);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
        }
    }

    public function gameCount($weekId,$tounamentId){
        $sql = "SELECT count(*) count FROM tournament t,fantasy f where t.id=f.tournamentId and t.id=:tournamentid";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':date', $date);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result[0]['count'];
    }

    public function status($username) {
        $Tournament=$this->getMatch('tournament');
        if($Tournament['type']=='fantasy'){
            $fixturesNext = $this->getFixtures($Tournament,'next');
            $fixturesDone = $this->getFixtures($Tournament,'done');
            if($fixturesNext)
                $fixturesMatch = $this->getFixturesMatch($fixturesNext);
            else $fixturesMatch = null;
            $players = $this->getPlayers($Tournament);
            $prediction = $this->getPrediction($Tournament,$username);
            if($fixturesNext)
                $transfer = $fixturesNext[0]['id']!=$prediction[0]['idMatch']?1:0;
            else $transfer=0;
            if($transfer){
                $mainPrediction = $prediction[0];
            }
            else $mainPrediction= array($prediction[0],$prediction[1]);
            $wallet = $this->walletStatus($username);
            $array = array('fixtures'=>$fixturesNext,'prediction'=>$mainPrediction,'isTransfer'=>$transfer,'player'=>$players,'fixtureMatch'=>$fixturesMatch,'walletStatus'=>$wallet);
            return $array;
        }
    }

    public function walletStatus($username){
        $sql = "SELECT Reset FROM nlwc where username = :username";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        if($result[0]['Reset']){
            return 0;
        }
        return 1;
    }

    public function walletReset($username){
        $sql = "UPDATE nlwc set Reset=:Reset,wallet=:wallet where username = :username";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $Reset=1;
        $wallet=100;
        $stmt->bindParam(':wallet', $wallet);
        $stmt->bindParam(':Reset', $Reset);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        return 1;
    }

    public function getFixtures($Tournament,$type=0) {
        $datetime = new DateTime();
        $date = $datetime->format('Y-m-d');
        if($type=='next'){
            $date = $datetime->format('Y-m-d H:i:s');
            $sql = "SELECT * FROM fantasy where startDate > :date and tournamentId = :tournamentId";
        }
        else if($type=='done'){
            $date = $datetime->format('Y-m-d H:i:s');
            $sql = "SELECT * FROM fantasy where startDate <= :date and tournamentId = :tournamentId";
        }
        else $sql = "SELECT * FROM fantasy where date(endDate) >= :date and tournamentId = :tournamentId limit 2";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':tournamentId', $Tournament['id']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getFixturesMatch($fixtures) {
        $date = new DateTime();
        $date = $date->format('Y-m-d');
        if($fixtures){
            $sqlId="(f.id=:fId0 or f.id=:fId1)";
        }
        else $sqlId="(f.id=:fId0)";
        $sql = "(SELECT title, team1, team2, i.date, i.time, description,f.id, 'Current' type FROM infomatch i, fantasy f WHERE i.tournamentId = f.tournamentId AND i.date BETWEEN f.startDate AND f.endDate AND $sqlId)";
        $stmt = $this->db->prepare($sql);
        if($fixtures){
            $next=$fixtures[0]['id'];
            $current=$next-1;
            $stmt->bindParam(':fId0', $current);
            $stmt->bindParam(':fId1', $next);
        }
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $mainData = array("next"=>array(),"current"=>array());
        $temp=null;
        foreach($result as $key=>$data){
            if($key==0)
                $temp=$data['id'];
            if($temp!=$data['id']){
                array_push($mainData['next'],$data);
            }
            else array_push($mainData['current'],$data);
        }
        return $mainData;
    }

    public function getPlayers($Tournament) {
        $sql = "SELECT * FROM players WHERE tournamentId =  :tournamentId order by team asc";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':tournamentId', $Tournament['id']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getMatch() {
        $date = new DateTime();
        $date = $date->format('Y-m-d');
        $sql = "SELECT * FROM `tournament` where date(startDate)<=:date and date(endDate)>=:date ORDER BY `id` DESC limit 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':date', $date);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result[0];
    }

    public function getPrediction($Tournament,$username) {
        $sql = "SELECT * FROM prediction where username=:username and tournamentId=:tournamentId order by idMatch desc";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':tournamentId', $Tournament['id']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }
}
