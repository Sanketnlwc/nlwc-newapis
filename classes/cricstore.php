<?php class cricstore {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getAlldata() {
         $sql = "( Select * From cricstore)";
        $stmt = $this->db->prepare($sql);
        $result= $stmt->execute();
        $er = $stmt -> errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function postOrder($data) {

        $sql = "UPDATE `cricstore` SET `count`=`count`+1 where `id`=:srno";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':srno', $data['srno']);
        $result = $stmt->execute();

        $sql = "insert into `".$data['srno']."_cricstore` (`username`,`phone`,`pincode`,`address1`,`locality`,`city`,`state`,`size`,`gender`,`qty`,`name`) values (:username,:phone,:pincode,:address1,:locality,:city,:state,:size,:gender,:qty,:name)";
             $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':username', $data['username']);
            $stmt->bindParam(':phone', $data['phone']);
            $stmt->bindParam(':pincode', $data['pincode']);
            $stmt->bindParam(':locality', $data['locality']);
            $stmt->bindParam(':address1', $data['address1']);
            $stmt->bindParam(':city', $data['city']);
            $stmt->bindParam(':state', $data['state']);
            $stmt->bindParam(':size', $data['size']);
            $stmt->bindParam(':gender', $data['gender']);
            $stmt->bindParam(':qty', $data['qty']);
            $stmt->bindParam(':name', $data['name']);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            if($result) 
            return 1;
            else
            return 0;
        
    }
}
?>