<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class predict {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getPredictMatches($username) {
        $date = new DateTime();
        $time = date("H:i:s");
        $date = $date->format('Y-m-d');
        $sql = "SELECT Sr_no,title, team1, team2, date, time  FROM infomatch where `update` = '0' order by `date` DESC";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        
        foreach($result as $key=>$value){
                 $srno = $value['Sr_no'];
                 $sql1 = "SELECT * FROM `".$srno."_prediction` where `username` = :username";
                 $stmt1 = $this->db->prepare($sql1);
                 $stmt1->bindParam(':username', $username);
                $result1 = $stmt1->execute();
                 $er1 = $stmt1->errorInfo();
                 $result1 = $stmt1->fetchAll();
                 if(count($result1)>0){
                     $result[$key]["isPredicted"] = true;
                 }
                 else{
                     $result[$key]["isPredicted"] = false;
                    }
                 }
        return $result;
    }

     public function getPredictFinishedMatches() {
        $date = new DateTime();
        $time = date("H:i:s");
        $date = $date->format('Y-m-d');
        $sql = "SELECT Sr_no,title, team1, team2, date, time,`update`  FROM infomatch  order by `date` DESC";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

     public function getMatchPoints() {
        $date = new DateTime();
        $time = date("H:i:s");
        $date = $date->format('Y-m-d');
        $sql = "SELECT Sr_no,title, team1, team2, date, time,`update`  FROM infomatch  where `update`!='0' order by `date` DESC limit 5";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

     public function getTournaments() {
        $sql = "SELECT Sr_no,name FROM tournament order by `Sr_no` DESC limit 5";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

public function getPredictMatchesDateTime($data) {
        $date = new DateTime();
        $time = date("H:i:s");
        $date = $date->format('Y-m-d');
        $sql = "SELECT Sr_no,title, team1, team2, date, time  FROM infomatch where `update` = '0' order by Sr_no LIMIT 3";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        

        return $result;
    }
    
    public function fetchPlayers($team1,$team2) {
        //$sql = "SELECT Player,Role, TeamName, Value,Points  FROM ipl2018_players where `TeamName` =:team1";
       $sql = "SELECT Player,Role, TeamName, Value,Points  FROM IPL2020_players where `TeamName` IN (:team1, :team2)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':team1', $team1);
        $stmt->bindParam(':team2', $team2);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
        
    }
    public function addPrediction($data) {
        //$sql = "SELECT Player,Role, TeamName, Value,Points  FROM ipl2018_players where `TeamName` =:team1";
                
date_default_timezone_set("Asia/Kolkata");
        $datetimesql = "select date,time from `infomatch` where Sr_no =".$data['srno'];
        $stmt1 = $this->db->prepare($datetimesql);
        $result1 = $stmt1->execute();
        $er1 = $stmt1->errorInfo();
        $result1 = $stmt1->fetchAll();
        if($result1[0]['date'] < date("Y-m-d") || ( $result1[0]['date'] == date("Y-m-d") && $result1[0]['time'] < date("H:i:s")))
            {
            return 2;
            }
         else{   
       $sql = "insert into `".$data['srno']."_prediction` (`username`,`player1`,`player2`,`player3`,`player4`,`player5`,`player6`,`player7`,`player8`,`player9`,`player10`,`player11`,`captain`,`points`,`team`) values (:username,:player1,:player2,:player3,:player4,:player5,:player6,:player7,:player8,:player9,:player10,:player11,:captain,0,:team)";
             $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':username', $data['username']);
            $stmt->bindParam(':player1', $data['player1']);
            $stmt->bindParam(':player2', $data['player2']);
            $stmt->bindParam(':player3', $data['player3']);
            $stmt->bindParam(':player4', $data['player4']);
            $stmt->bindParam(':player5', $data['player5']);
            $stmt->bindParam(':player6', $data['player6']);
            $stmt->bindParam(':player7', $data['player7']);
            $stmt->bindParam(':player8', $data['player8']);
            $stmt->bindParam(':player9', $data['player9']);
            $stmt->bindParam(':player10', $data['player10']);
            $stmt->bindParam(':player11', $data['player11']);
            $stmt->bindParam(':captain', $data['captain']);
            $stmt->bindParam(':team', $data['team']);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            if($result) 
            return 1;
            else
            return 0;
            }
        
    }
    public function updatePrediction($data) {
        //$sql = "SELECT Player,Role, TeamName, Value,Points  FROM ipl2018_players where `TeamName` =:team1";
           
date_default_timezone_set("Asia/Kolkata");
        $datetimesql = "select date,time from `infomatch` where Sr_no =".$data['srno'];
        $stmt1 = $this->db->prepare($datetimesql);
        $result1 = $stmt1->execute();
        $er1 = $stmt1->errorInfo();
        $result1 = $stmt1->fetchAll();
        if($result1[0]['date'] < date("Y-m-d") || ( $result1[0]['date'] == date("Y-m-d") && $result1[0]['time'] < date("H:i:s")))
            {
            return 2;
            }
         else{  
       $sql = "update `".$data['srno']."_prediction` set  `player1`=:player1,`player2`=:player2,`player3`=:player3,`player4`=:player4,`player5`=:player5,`player6`=:player6,`player7`=:player7,`player8`=:player8,`player9`=:player9,`player10`=:player10,`player11`=:player11,`captain`=:captain,`team`=:team where `username`=:username";
             $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':username', $data['username']);
            $stmt->bindParam(':player1', $data['player1']);
            $stmt->bindParam(':player2', $data['player2']);
            $stmt->bindParam(':player3', $data['player3']);
            $stmt->bindParam(':player4', $data['player4']);
            $stmt->bindParam(':player5', $data['player5']);
            $stmt->bindParam(':player6', $data['player6']);
            $stmt->bindParam(':player7', $data['player7']);
            $stmt->bindParam(':player8', $data['player8']);
            $stmt->bindParam(':player9', $data['player9']);
            $stmt->bindParam(':player10', $data['player10']);
            $stmt->bindParam(':player11', $data['player11']);
            $stmt->bindParam(':captain', $data['captain']);
            $stmt->bindParam(':team', $data['team']);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
            if($result) 
            return 1;
            else
            return 0;
         }
    }

       public function fetchTeam($data) {
        //$sql = "SELECT Player,Role, TeamName, Value,Points  FROM ipl2018_players where `TeamName` =:team1";
       $sql = "SELECT *  FROM `".$data['srno']."_prediction` where `username`=:username order by `points` DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $data['username']);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result[0];
        
    }

    public function fetchPredictions($srno,$type) {
        //$sql = "SELECT Player,Role, TeamName, Value,Points  FROM ipl2018_players where `TeamName` =:team1";
        if($type=="match")
            $sql = "SELECT *  FROM `".$srno."_prediction` order by `points` DESC";
        else
            $sql = "SELECT *  FROM `".$srno."_tournament` order by `points` DESC";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
        
    }
}
