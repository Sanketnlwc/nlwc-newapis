<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class enroll {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function subscribe($userId, $courseId, $paymentType) {
        $sql="SELECT * FROM `enrollment` where `userId`=:user_id and `courseId`=:courseId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':courseId', $courseId);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        if (count($result) == 0) {
            $sql = "insert into `enrollment` (`userId`,`courseId`,`paymentType`) values (:user_id,:courseId,:paymentType)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':user_id', $userId);
            $stmt->bindParam(':courseId', $courseId);
            $stmt->bindParam(':paymentType', $paymentType);
            $result = $stmt->execute();
            $id = $this->db->lastInsertId();
            $er = $stmt->errorInfo();
            $stmt->closeCursor();
        }
        else {
            $id=$result[0]['id'];
        }
        return $id;
    }

    public function add($userId, $enrollmentId, $payment) {
        $sql = "SELECT count(*) FROM `coursePayment` where `enrollmentId`=:enroll";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':enroll', $enrollmentId);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        $sql = "insert into `coursePayment` (`userId`,`enrollmentId`,`payment`,`installment`) values (:user_id,:enrollmentId,:payment,:installment)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':enrollmentId', $enrollmentId);
        $stmt->bindParam(':payment', $payment);
        $installment = $result[0]['count(*)']+1;
        $stmt->bindParam(':installment', $installment);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $stmt->closeCursor();
        return 'success';
    }

}
