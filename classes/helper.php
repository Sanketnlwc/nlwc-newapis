<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class helper {

    public function profilePic($data){
        $url = "https://www.nolifewithoutcricket.com";
        foreach($data as $index=>$innerData){
            if($innerData['profile_pic']){
                if(substr($innerData['profile_pic'],0,5)!=='https'){
                    $data[$index]['profile_pic']=$url.$innerData['profile_pic'];
                }
            }
        }
        return $data;
    }

    public function curlFunction1($url){
        $response = file_get_contents($url);
        return $response;
    }

    public function curlFunction($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        $result = curl_exec($curl);
        $redirectURL = curl_getinfo($curl,CURLINFO_EFFECTIVE_URL );
        print_r($redirectURL);
        $err = curl_error($curl);
        curl_close($curl);
        return $result;
    }

    public function curlFunction2($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER=> false,
            CURLOPT_SSL_VERIFYHOST=> false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        print_r($err);

        curl_close($curl);

        return $response;
  }
}
