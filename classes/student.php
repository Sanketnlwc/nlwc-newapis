<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class student {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function retrieve($name=null) {
            $sql = "SELECT s.*,(SELECT group_concat(concat(baseStandard,'-',courseName)) FROM `courses` c WHERE FIND_IN_SET(c.courseId, e.courseId) GROUP BY FIND_IN_SET(e.courseId, c.courseId)) courses,e.paymentType,e.active,sum(p.payment) as totalPaid,(select sum(fees)-sum(p.payment) from `courses` c where find_in_set(c.courseId,e.courseId) group by find_in_set(e.courseId,c.courseId)) balance FROM `studentDB` s,`enrollment` e,`coursePayment` p where s.userId=e.userId and e.id=p.enrollmentId and (s.firstName like :firstName or s.lastName like :lastName or s.userId like :lastName) group by p.enrollmentId";
            $stmt = $this->db->prepare($sql);
            $name = "%".$name."%";
            $stmt->bindParam(':firstName', $name);
            $stmt->bindParam(':lastName', $name);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $result = $stmt->fetchAll();
            return $result;
    }

    public function payment() {
            $sql = "select * from (SELECT s.userId,s.firstName,s.lastName,s.studentMobile,s.parentMobile,s.emailId,(SELECT group_concat(concat(baseStandard,'-',courseName)) FROM `courses` c WHERE FIND_IN_SET(c.courseId, e.courseId) GROUP BY FIND_IN_SET(e.courseId, c.courseId)) courses,e.paymentType,e.active,SUM(p.payment) AS totalPaid,(SELECT SUM(fees) - SUM(p.payment) FROM `courses` c WHERE FIND_IN_SET(c.courseId, e.courseId) GROUP BY FIND_IN_SET(e.courseId, c.courseId)) balance FROM `studentDB` s,`enrollment` e, `coursePayment` p WHERE s.userId = e.userId AND e.id = p.enrollmentId GROUP BY p.enrollmentId)a where balance>0";
            $stmt = $this->db->prepare($sql);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $result = $stmt->fetchAll();
            return $result;
    }

    public function getBatch($batchId,$standard) {
            $sql = "select * from (SELECT s.userId,s.firstName,s.lastName,s.studentMobile,s.parentMobile,s.emailId,(SELECT group_concat(concat(baseStandard,'-',courseName)) FROM `courses` c WHERE FIND_IN_SET(c.courseId, e.courseId) GROUP BY FIND_IN_SET(e.courseId, c.courseId)) courses,e.paymentType,e.active,SUM(p.payment) AS totalPaid,(SELECT SUM(fees) - SUM(p.payment) FROM `courses` c WHERE FIND_IN_SET(c.courseId, e.courseId) GROUP BY FIND_IN_SET(e.courseId, c.courseId)) balance FROM `studentDB` s,`enrollment` e, `coursePayment` p WHERE s.userId = e.userId AND e.id = p.enrollmentId GROUP BY p.enrollmentId)a where balance>0";
            $stmt = $this->db->prepare($sql);
            $result = $stmt->execute();
            $er = $stmt->errorInfo();
            $result = $stmt->fetchAll();
            return $result;
    }
}
