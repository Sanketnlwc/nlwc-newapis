<?php

$data = "<html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <title>Email Template</title>
    </head>
    <body>
        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
            <tbody>
                <tr>
                    <td style='padding:10px 0 30px 0'>
                        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border:1px solid #cccccc; border-collapse:collapse'>
                            <tbody>
                                <tr>
                                    <td align='center' bgcolor='#70bbd9' style='padding:40px 0 30px 0; color:#153643; font-size:28px; font-weight:bold; font-family:Arial,sans-serif'>
                                        <img src='https://www.nolifewithoutcricket.com/img/new_logo.png' alt='NLWC Logo' width='300' height='230' style='display:block'> </td>
                                </tr>
                                <tr>
                                    <td bgcolor='#ffffff' style='padding:40px 30px 40px 30px'>
                                        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                            <tbody>
                                                <tr>
                                                    <td style='color:#153643; font-family:Arial,sans-serif; font-size:24px; text-align: center'><b>No Life Without Cricket</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                            <tbody>
                                                                <tr>
                                                                    <td style='color:#153643; font-family:Arial,sans-serif; font-size:22px'>
                                                                        <hr>
                                                                            <br>" . $main . "<br>
                                                                                    Regards, <br>
                                                                                        Team NLWC <br>
                                                                                            </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                            </table>
                                                                                            </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                            </table>
                                                                                            </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor='#ee4c50' style='padding:30px 30px 30px 30px'>
                                                                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td width='75%' style='color:#ffffff; font-family:Arial,sans-serif; font-size:14px'>
                                                                                                                    Follow Us On : Facebook Twitter <br>
                                                                                                                        <br>
                                                                                                                            For Any Queries And Feedback Contact Us At : contact@nolifewithoutcricket.com </td>
                                                                                                                            <td align='right' width='25%'>
                                                                                                                                <table border='0' cellpadding='0' cellspacing='0'>
                                                                                                                                    <tbody>
<tr>
	<td style='font-family:Arial,sans-serif; font-size:12px; font-weight:bold'><a href='http://www.twitter.com/NLWC07' target='_blank' style='color:#ffffff'><img src='http://nolifewithoutcricket.com/img/twitter.png' alt='Twitter' width='38' height='38' border='0' style='display:block'> </a></td>
	<td width='20' style='font-size:0; line-height:0'>&nbsp;</td>
	<td style='font-family:Arial,sans-serif; font-size:12px; font-weight:bold'><a href='http://www.facebook.com/NLWC07' target='_blank' style='color:#ffffff'><img src='http://nolifewithoutcricket.com/img/fb.png' alt='Facebook' width='38' height='38' border='0' style='display:block'> </a></td>
	<td width='20' style='font-size:0; line-height:0'>&nbsp;</td>
	<td style='font-family:Arial,sans-serif; font-size:12px; font-weight:bold'><a href='https://www.instagram.com/nolifewithoutcricket/' target='_blank' style='color:#ffffff'><img src='http://nolifewithoutcricket.com/img/instagram.png' alt='Instagram' width='38' height='38' border='0' style='display:block'> </a></td>
	
	</tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                            </table>
                                                                                                                            </td>
                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                            </table>
                                                                                                                            </td>
                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                            </table>
                                                                                                                            </body>
                                                                                                                            </html>";
?>