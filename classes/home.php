<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class home {

    protected $db;

    function __construct($db = null) {
        try {
            $this->db = $db;
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        } catch (Exception $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        }
    }

    public function getAlldata() {
        // $blogs = $this->getBlogs();
        //  $MatchDetails=$this->getMatch();
         $predictions = $this->getMatchPrediction();
         $tournamentPredictions = $this->getTournamentPrediction();
        // $Tournament=$this->getMatch('tournament');
        // $tourPredictions = $this->getPrediction($Tournament['id'],'tournament',5);
        // $tournamentList = $this->getMatchList('tournament');
        // $matchList = $this->getMatchList();
        // $players = $this->getTopPlayers($Tournament);
        // $bannerInfo = $this->getBannerInfo();
        $blogdata = $this->getBlogdata();
        $array = array("BlogData"=>$blogdata,'matchPredictions'=>$predictions,'TournamentPrediction'=>$tournamentPredictions);
            // 'Blogs'=>$blogs,'match'=>$MatchDetails,'matchPrediction'=>$predictions,'tournament'=>$Tournament,'tournamentPrediction'=>$tourPredictions,'tourList'=>$tournamentList,'matchList'=>$matchList,"players"=>$players,"bannerInfo"=>$bannerInfo,"BlogData"=>$blogdata);
       return $array;
    }

    public function getMatchList($type='single') {
        if($type=='single')
            $sql = "SELECT id,description,startDate,endDate FROM `fantasy`  where (startDate<=:date) ORDER BY `startDate` DESC limit 5";
        else $sql = "SELECT id,name FROM `tournament` ORDER BY `id` DESC limit 7";
        $stmt = $this->db->prepare($sql);
        if($type=="single"){
            $date=date('Y-m-d H:i:s');
            $stmt->bindParam(':date', $date);
        }
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getBlogs() {
        $sql = "SELECT `fname`,`lname`,`profile_pic`,`title`,`points` FROM `blogs` b,`nlwc` n where n.username=b.username ORDER BY `points` DESC limit 10";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        return $result;
    }

    public function getMatchPrediction() {
            $sql = "SELECT Sr_no,team1,team2 FROM `infomatch` where `update`!= '0' ORDER BY `Sr_no` DESC limit 1";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
            $matchPredictions = "Select * from `".$result[0]['Sr_no']."_prediction` ORDER BY `points` DESC";
        $stmt1 = $this->db->prepare($matchPredictions);
        $result1 = $stmt1->execute();
        $er = $stmt1->errorInfo();
        $result1 = $stmt1->fetchAll();
        $result1[0]['team1'] = $result[0]['team1'];
        $result1[0]['team2'] = $result[0]['team2'];
        return $result1;
        
    }

    public function getTournamentPrediction(){
         $sql = "SELECT Sr_no FROM `tournament` ORDER BY `Sr_no` DESC limit 1";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
            $matchPredictions = "Select * from `".$result[0]['Sr_no']."_tournament` ORDER BY `points` DESC";
        $stmt1 = $this->db->prepare($matchPredictions);
        $result1 = $stmt1->execute();
        $er = $stmt1->errorInfo();
        $result1 = $stmt1->fetchAll();

        return $result1;
    }
    public function tornamentManipulation($id,$type){
        $data = $this->getPrediction($id,$type);
    }

    public function getPrediction($id,$type="prediction",$limit=null) {
        if($type=='prediction')
            $sql = "Select fname,lname,n.username,player,profile_pic,points,team from prediction p,nlwc n where p.username=n.username and p.idMatch=:id ORDER BY `points` desc";
        else if($type=='fantasy')
            $sql = "Select fname,lname,n.username,player,profile_pic,points,team from prediction p,nlwc n where p.username=n.username and p.idMatch=:id ORDER BY `points` desc";
        else if($type=='prev')
            $sql = "Select n.username from prediction p,nlwc n where p.username=n.username and p.idMatch=:id ORDER BY `points` desc";
        else $sql = "Select fname,lname,n.username,profile_pic,sum(points) points from prediction p,nlwc n where p.username=n.username and p.tournamentId=:id  GROUP BY p.username,p.tournamentId ORDER BY `points` desc";
        if($limit)
            $sql .= " limit 10";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
        $result = $helper->profilePic($result);
        if($type=='prediction')
            $result = $this->rankManipulation($result,$id);
        return $result;
    }

    public function arrayUsers($data){
        $main=[];
        foreach($data as $innerData){
            array_push($main,$innerData['username']);
        }
        return $main;
    }

    public function rankManipulation($result,$id){
        $prevData = $this->getPrediction($id-1,'prev');
        $prevData = $this->arrayUsers($prevData);
        $mainData=[];
        foreach($result as $key=>$innerData){
            $rank = array_search($innerData['username'],$prevData);
            if($rank>$key)
                $innerData['rank']='forw';
            else if($rank<$key)
                $innerData['rank']='back';
            else $innerData['rank']='-';
            array_push($mainData,$innerData);
        }
        return $mainData;
    }

    public function getTopPlayers($Tournament) {
        
        $sql = "(SELECT * FROM  players WHERE `role` = 'BOWL' ORDER BY  `totalPoints` DESC LIMIT 4) UNION (SELECT * FROM  players WHERE `role` = 'BAT' ORDER BY  `totalPoints` DESC LIMIT 4) UNION (SELECT * FROM players WHERE `role` = 'ALL' ORDER BY `totalPoints` DESC LIMIT 2) UNION (SELECT * FROM players WHERE `role` = 'WK' ORDER BY  `totalPoints` DESC LIMIT 1)";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getBannerInfo(){

        $sql = "( Select * From bannerinfo)";
        $stmt = $this->db->prepare($sql);
        $result= $stmt->execute();
        $er = $stmt -> errorInfo();
        $result = $stmt->fetchAll();
        return $result;
    }


     public function getBlogdata() {
            $sql = "select fname,lname,profile_pic,title,b.Sr_No blogSRNO,`date`,`like`,`views` from blogs b,nlwc n where n.username=b.username and b.News=0 order by b.Sr_No desc limit 5";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        $er = $stmt->errorInfo();
        $result = $stmt->fetchAll();
        $helper = new helper();
      //  $result = $helper->profilePic($result);
        $mainUrl = "http://www.nolifewithoutcricket.com/";
        foreach($result as $index=>$innerData){
            $url = "../blogs/".$innerData['title'].".txt";
            $fh = fopen($url,'r');
            $blog = fread($fh,filesize($url));
            fclose($fh);
            $blog = strip_tags($blog);
            $str = $this->myTruncate($blog);
            $result[$index]['blogData']=$str;
        }
        return $result;
  }

  public function myTruncate($input, $numwords="30", $padding="...<a class='link'>Readmore</a>"){
    $output = strtok($input, " \n");
    while(--$numwords > 0) $output .= " " . strtok(" \n");
    // if($output != $input) $output .= $padding;
    return $output;
  }
}
