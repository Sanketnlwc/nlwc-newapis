<?php
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$config['db']['host']   = getenv('DB_URL');
$config['db']['user']   = getenv('DB_USERNAME');
$config['db']['pass']   = getenv('DB_PASSWORD');
$config['db']['dbname'] = getenv('DB_NAME');
$config['logger'] = [
            'name' => 'slim-app',
            'path' => __DIR__ . '../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ];
return $config;